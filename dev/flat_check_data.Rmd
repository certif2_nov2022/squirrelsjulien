---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)

data("data_act_squirrels")
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok 

This function check if all the colors in the input parameters (string), is in the list Gray, Cinnamon, Black.

    
```{r function-check_primary_color_is_ok }
#' check_primary_color_is_ok
#' 
#' Function that check if the color is in the list
#' 
#' @param string Character. Color vector
#' 
#' @return Boolean. TRUE if all colors are ok
#' 
#' @export
check_primary_color_is_ok  <- function(string){
     all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
     if(isTRUE(all_colors_OK)){
       return(all_colors_OK)
     }
     else{
       stop("The color is not in the list 'Gray', 'Cinnamon', 'Black' or NA")
     }
  
}
```
  
```{r example-check_primary_color_is_ok }
check_primary_color_is_ok ("Black")
check_primary_color_is_ok (NA)
check_primary_color_is_ok(c(NA, "Black", "Gray", "Cinnamon"))

```
  
```{r tests-check_primary_color_is_ok }
test_that("check_primary_color_is_ok  works", {
  expect_true(inherits(check_primary_color_is_ok , "function"))
  
  expect_true(object = check_primary_color_is_ok ("Black"))
  expect_error(object = check_primary_color_is_ok ("Cyan"), "The color is not in the list 'Gray', 'Cinnamon', 'Black' or NA")
})
```


# check_squirrel_data_integrity

This function check if a dataset as a column primary_fur_color and if all colors are in the list NA, 'Black', 'Gray' and 'Cinnamon
    
```{r function-check_squirrel_data_integrity}
#' Check Squirrel Data Integrity: check_squirrel_data_integrity
#' 
#' Check if a dataset as a column primary_fur_color and if all colors are in the list NA, 'Black', 'Gray' and 'Cinnamon
#' 
#' @param dataset data.frame. Dataset that you want to check
#' 
#' @return Message if all color in the column primary_fur_color of my dataset
#' 
#' @export
check_squirrel_data_integrity <- function(dataset){
  
  if(isFALSE("primary_fur_color" %in% names(dataset))){
    stop("'primary_fur_color' must be a column of your dataset")
  }
  else{
    if(isTRUE(check_primary_color_is_ok(dataset[['primary_fur_color']]))){
      message("The column 'primary_fur_color' is OK")
    }
  }
    
}
```
  
```{r example-check_squirrel_data_integrity}
data("data_act_squirrels")
check_squirrel_data_integrity(data_act_squirrels)

data_test_2 <- readr::read_csv(system.file("nyc_squirrels_sample.csv", package = "squirrelsjulien"))

check_squirrel_data_integrity(data_test_2)

```
  
```{r tests-check_squirrel_data_integrity}
test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function"))
  
  
  data_error_1 <- data.frame(column_A = c(1, 2, 5, 7,  10),
                           column_B = c("A", "B", "E", "G", "J"))
  expect_error(check_squirrel_data_integrity(data_error))
  
  data_error_2 <- data.frame(id_squirrels = c(1, 2, 3, 4, 5),
                           primary_fur_color = c("Black", NA, "Blue", "Red", "Cinnamon"))
  expect_error(check_squirrel_data_integrity(data_error_2))
  
  data_message <- data.frame(id_squirrels = c(1, 2, 3, 4),
                           primary_fur_color = c(NA, "Black", "Gray", "Cinnamon"))
  expect_message(check_squirrel_data_integrity(data_message))
  
})
```
  

  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_check_data.Rmd", vignette_name = "Check data")
```



<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsjulien

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsjulien is to …

## Installation

You can install the development version of squirrelsjulien like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsjulien)
## basic example code
```

# Get a message with a fur color: get_message_fur_color

You can get a message with the fur color of interest with
`get_message_fur_color()`.

``` r
get_message_fur_color("bleu")
#> We will focus on bleu squirrels
```

# Check Primary Color is OK : check_primary_color_is_ok

This function check if all the colors in the input parameters (string),
is in the list Gray, Cinnamon, Black.

``` r
check_primary_color_is_ok ("Black")
#> [1] TRUE
check_primary_color_is_ok (NA)
#> [1] TRUE
check_primary_color_is_ok(c(NA, "Black", "Gray", "Cinnamon"))
#> [1] TRUE
```

# Check Squirrel Data Integrity: check_squirrel_data_integrity

This function check if a dataset as a column primary_fur_color and if
all colors are in the list NA, ‘Black’, ‘Gray’ and ’Cinnamon

``` r
data("data_act_squirrels")
check_squirrel_data_integrity(data_act_squirrels)
#> The column 'primary_fur_color' is OK

data_test_2 <- readr::read_csv(system.file("nyc_squirrels_sample.csv", package = "squirrelsjulien"))
#> New names:
#> • `` -> `...1`
#> Rows: 400 Columns: 37
#> ── Column specification ────────────────────────────────────────────────────────
#> Delimiter: ","
#> chr (14): unique_squirrel_id, hectare, shift, age, primary_fur_color, highli...
#> dbl (10): ...1, long, lat, date, hectare_squirrel_number, zip_codes, communi...
#> lgl (13): running, chasing, climbing, eating, foraging, kuks, quaas, moans, ...
#> 
#> ℹ Use `spec()` to retrieve the full column specification for this data.
#> ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

check_squirrel_data_integrity(data_test_2)
#> The column 'primary_fur_color' is OK
```

---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# squirrelsjulien

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsjulien is to ...

## Installation

You can install the development version of squirrelsjulien like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(squirrelsjulien)
## basic example code
```


# Get a message with a fur color: get_message_fur_color

You can get a message with the fur color of interest with `get_message_fur_color()`.

```{r}
get_message_fur_color("bleu")
```



# Check Primary Color is OK : check_primary_color_is_ok 

This function check if all the colors in the input parameters (string), is in the list Gray, Cinnamon, Black.

```{r example-check_primary_color_is_ok }
check_primary_color_is_ok ("Black")
check_primary_color_is_ok (NA)
check_primary_color_is_ok(c(NA, "Black", "Gray", "Cinnamon"))

```


# Check Squirrel Data Integrity: check_squirrel_data_integrity

This function check if a dataset as a column primary_fur_color and if all colors are in the list NA, 'Black', 'Gray' and 'Cinnamon

```{r example-check_squirrel_data_integrity}
data("data_act_squirrels")
check_squirrel_data_integrity(data_act_squirrels)

data_test_2 <- readr::read_csv(system.file("nyc_squirrels_sample.csv", package = "squirrelsjulien"))

check_squirrel_data_integrity(data_test_2)

```




